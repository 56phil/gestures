//
//  ViewController.m
//  Wk6GestureRecongizer
//
//  Created by Sai Chow on 6/24/15.
//  Copyright (c) 2015 some company. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UIGestureRecognizerDelegate>

@property (nonatomic, strong) UITapGestureRecognizer *tapRecognizer;
@property (nonatomic, strong) UITapGestureRecognizer *tapRecognizer2;
@property (nonatomic, strong) UIPanGestureRecognizer *panRecognizer;
@property (nonatomic, strong) UISwipeGestureRecognizer *swipeRecognizer;
@property (nonatomic, strong) UIView *someOtherView;
@property (nonatomic, assign) BOOL didSwipe;

@property (nonatomic, strong) IBOutlet UIScreenEdgePanGestureRecognizer *screenEdgeREcongnizer;

@end

@implementation ViewController

- (void)setupTapRelatedGestures {
    //tap
    self.tapRecognizer = [[UITapGestureRecognizer alloc] init];
    [self.tapRecognizer addTarget:self action:@selector(handleTapRecognizer:)];
    self.tapRecognizer.delegate = self;
    
    [self.view addGestureRecognizer:self.tapRecognizer];
    NSLog(@"tap recog 1: %@", self.tapRecognizer);
    [self.someOtherView addGestureRecognizer:self.tapRecognizer];
    
    
    //multiple tap recognizer
    self.tapRecognizer2 = [[UITapGestureRecognizer alloc] init];
    NSLog(@"tap recog 2: %@", self.tapRecognizer2);
    self.tapRecognizer2.delegate = self;
    [self.tapRecognizer2 addTarget:self action:@selector((handleTapRecognizer:))];
    
    [self.someOtherView addGestureRecognizer:self.tapRecognizer2];
}

- (void)setupSwipeRecognizer {
    //swipe
    self.swipeRecognizer = [[UISwipeGestureRecognizer alloc] init];
    self.swipeRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.swipeRecognizer addTarget:self action:@selector(handleSwipe:)];
    [self.view addGestureRecognizer:self.swipeRecognizer];
}

- (void)setupGestureRecognizer {
    [self setupTapRelatedGestures];
    [self setupSwipeRecognizer];
    
    //panning
    self.panRecognizer = [[UIPanGestureRecognizer alloc] init];
    [self.panRecognizer addTarget:self action:@selector(handlePanningRecognizer:)];
    
    self.panRecognizer.delegate = self;
    [self.someOtherView addGestureRecognizer:self.panRecognizer];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.someOtherView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    [self.someOtherView setBackgroundColor:[UIColor greenColor]];

    [self.view addSubview:self.someOtherView];

    [self setupGestureRecognizer];
}

//- (void)handleGesture:(UIGestureRecognizer *)gesture {
//    if ([gesture isKindOfClass:[UISwipeGestureRecognizer class]]) {
//        
//    } else if ([gesture isKindOfClass:[UIPanGestureRecognizer class]]) {
//        
//    }
//}

- (IBAction)handleScreenEdge:(id)sender {
    NSLog(@"handleScreenEdge: %@", sender);
}

- (void)handleSwipe:(UISwipeGestureRecognizer *)gesture {
    [UIView animateWithDuration:1.0f animations:^{
        if (!self.didSwipe) {
            [self.someOtherView setAlpha:.25f];
        } else {
            [self.someOtherView setAlpha:1.0f];
        }
//        [self.someOtherView setAlpha:self.didSwipe ? .25f : 1.0f];
        
    } completion:^(BOOL finished) {
        self.didSwipe = !self.didSwipe;
//        [UIView animateWithDuration:1.0f animations:^{
//            [self.someOtherView setAlpha:1.0f];
//            
//        } completion:^(BOOL finished) {
//            
//            [self.view removeGestureRecognizer:gesture];
//            
//        }];
    }];

}

- (void)handleTapRecognizer:(UITapGestureRecognizer *)gesture {
    NSLog(@"handleTapRecognizer %@", gesture);
}

- (void)handlePanningRecognizer:(UIPanGestureRecognizer *)gesture {
    NSLog(@"handlePanningRecognizer");
    CGPoint touchPoint = [gesture locationInView:self.someOtherView];
    NSLog(@"Touch Point: x: %f, y: %f", touchPoint.x, touchPoint.y);
    self.someOtherView.center = touchPoint;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// called when a gesture recognizer attempts to transition out of UIGestureRecognizerStatePossible. returning NO causes it to transition to UIGestureRecognizerStateFailed
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    
//    NSLog(@"gestureRecognizerShouldBegin");
//    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
//
//        
//        NSLog(@"UIGestureRecognizerStateBegan");
//    }
//    
//    if (gestureRecognizer.state == UIGestureRecognizerStateRecognized) {
//        NSLog(@"UIGestureRecognizerStateRecognized");
//    }
    return YES;
}

// called when the recognition of one of gestureRecognizer or otherGestureRecognizer would be blocked by the other
// return YES to allow both to recognize simultaneously. the default implementation returns NO (by default no two gestures can be recognized simultaneously)
//
// note: returning YES is guaranteed to allow simultaneous recognition. returning NO is not guaranteed to prevent simultaneous recognition, as the other gesture's delegate may return YES
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    NSLog(@"gestureRecognizer");
    
    return YES;
}

// called once per attempt to recognize, so failure requirements can be determined lazily and may be set up between recognizers across view hierarchies
// return YES to set up a dynamic failure requirement between gestureRecognizer and otherGestureRecognizer
//
// note: returning YES is guaranteed to set up the failure requirement. returning NO does not guarantee that there will not be a failure requirement as the other gesture's counterpart delegate or subclass methods may return YES
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRequireFailureOfGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    NSLog(@"gestureRecognizer");
    
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    NSLog(@"gestureRecognizer");
    
    return YES;
 
}

// called before touchesBegan:withEvent: is called on the gesture recognizer for a new touch. return NO to prevent the gesture recognizer from seeing this touch
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    NSLog(@"gestureRecognizer");

    return YES;
}

@end
