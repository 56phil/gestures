//
//  AppDelegate.h
//  Wk6GestureRecongizer
//
//  Created by Sai Chow on 6/24/15.
//  Copyright (c) 2015 some company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

