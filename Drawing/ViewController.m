//
//  ViewController.m
//  Drawing
//
//  Created by Ryan D Taylor on 6/20/15.
//  Copyright (c) 2015 Disruption Institute. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UILongPressGestureRecognizer *longPressGesture;
@property UIImageView *rocketship;
@property (weak, nonatomic) IBOutlet UIImageView *aFace;
@property UITapGestureRecognizer *doubleTapGestureRecognizer;
@property (weak, nonatomic) IBOutlet UIImageView *longPressRecongnizers;
@property (strong, nonatomic) NSArray* pix;
@property (nonatomic) unsigned pressCounter;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self setupRocketship:@"RedRocket"
//                   height:141.4];
    [self setupGestureRecognizers];
    self.pix = @[[UIImage imageNamed:@"jt"], [UIImage imageNamed:@"nc"]];
    self.pressCounter = 0;
}
- (IBAction)longPressOnFace:(id)sender {
    
    self.pressCounter++;
    
    self.aFace.image = [self.pix objectAtIndex:self.pressCounter % 2];
}

- (void)setupRocketship:(NSString *)rocketshipName
                 height:(CGFloat)rocketshipHeight {
    UIImage *rocketshipImage = [UIImage imageNamed:rocketshipName];
    self.rocketship = [[UIImageView alloc] initWithImage:rocketshipImage];
    // resize the image view
    [self.rocketship setFrame:CGRectMake(100.0,
                                         self.view.frame.size.height - rocketshipHeight,
                                         rocketshipHeight / 2,
                                         rocketshipHeight)];
    [self.view addSubview:self.rocketship];
}

- (void)setupGestureRecognizers {
    if (self.rocketship != nil) {
        /** add a "swipe up" gesture recognizer to the rocketship to launch the rocketship **/
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]
                                                            initWithTarget:self
                                                            action:@selector(launchRocketship:)];
        // number of fingers that must be on the screen
        swipeGestureRecognizer.numberOfTouchesRequired = 1;
        swipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionUp;
        
        self.rocketship.userInteractionEnabled = YES;
        [self.rocketship addGestureRecognizer:swipeGestureRecognizer];
        
        /** add a "double tap" gesture recognizer to the view controller's view to land the rocketship **/
        self.doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]
                                           initWithTarget:self
                                           action:@selector(landRocketship:)];
        self.doubleTapGestureRecognizer.numberOfTapsRequired = 2;
        self.doubleTapGestureRecognizer.delegate = self;
        [self.view addGestureRecognizer:self.doubleTapGestureRecognizer];
    }
    
    /** the "rotation" gesture recognizer for Nicolas Cage was added to the ImageView using Interface Builder **/
    // self.cageFace.userInteractionEnabled = YES; This has been set in the Property Inspector in Interface Builder
    self.aFace.center = self.view.center;
    
}

- (void)launchRocketship:(UISwipeGestureRecognizer *)recognizer {
    // disallow the user to land the rocketship while it's launching
    self.doubleTapGestureRecognizer.enabled = NO;
    [UIView animateWithDuration:5.0 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        // animate the rocketship's position up the y-axis
        CGRect frame = self.rocketship.frame;
        frame.origin.y = 0 - frame.size.height;
        self.rocketship.frame = frame;
        
        // fade out the rocketship's transparency
        self.rocketship.alpha = 0.0;
    } completion:^(BOOL finished) {
        // the rocket has launched successfully; allow the user to land the rocketship
        self.doubleTapGestureRecognizer.enabled = YES;
        finished ? NSLog(@"%@", @"The launch was a success!") : NSLog(@"%@", @"The launch did not complete.");
    }];
}

- (void)landRocketship:(UITapGestureRecognizer *)recognizer {
    [UIView animateWithDuration:5.0 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate the rocketship's position down the y-axis
        CGRect frame = self.rocketship.frame;
        frame.origin.y = self.view.frame.size.height - self.rocketship.frame.size.height;
        self.rocketship.frame = frame;
        
        // fade in the rocketship's transparency
        self.rocketship.alpha = 1.0;
    } completion:^(BOOL finished) {
        // disawllow the user to land the rocketship as it has already landed
        self.doubleTapGestureRecognizer.enabled = NO;
        finished ? NSLog(@"%@", @"The landing was a success!") : NSLog(@"%@", @"The landing did not complete.");
    }];
}

- (IBAction)spinFace:(UIRotationGestureRecognizer *)recognizer {
    self.aFace.transform = CGAffineTransformMakeRotation(recognizer.rotation);
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        recognizer.rotation += recognizer.rotation;
    }
    /* to perform a rotation gesture in the iOS simulator,
     hold down option, click the view that you want to rotate, and "circle" your cursor */
}

#pragma mark - UIGestureRecognizerDelegate methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if (touch.tapCount == 2) {
        NSLog(@"A double tap has occurred.");
    }
    return YES;
}

@end
