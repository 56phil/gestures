//
//  main.m
//  Drawing
//
//  Created by Ryan D Taylor on 6/20/15.
//  Copyright (c) 2015 Disruption Institute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
